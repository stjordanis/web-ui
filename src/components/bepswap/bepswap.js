import React from 'react';
import { renderRoutes } from 'react-router-config';
import { NavTab } from 'react-router-tabs';
import Paper from "@material-ui/core/Paper/Paper";
import './routerTabs.scss';
import Divider from '@material-ui/core/Divider';
import AppBar from "@material-ui/core/AppBar/AppBar";

export default ({ routes }) => (
    <AppBar position="static" style={{boxShadow: 'none',
        backgroundColor: '#fff',borderRadius: 20}}>
        <Paper style={{
            borderRadius: 20,flexGrow: 1,
            boxShadow: '0 2px 12px 0 rgba(0,0,0,0.14)', paddingBottom: 10}}>
            {routes.filter(route => route.tab).map(({ path, tab }, i) => (
                <NavTab key={`tab_${i}`} to={path} >
                    {tab}
                </NavTab>
            ))}
            <Divider/>
            {renderRoutes(routes)}
        </Paper>
    </AppBar>
);
