import {Component} from "react";
import {withStyles} from "@material-ui/core";
import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid/Grid";
import Avatar from "@material-ui/core/Avatar/Avatar";
import assetUser from "../../../assets/assetUser.svg";
import assetArrowTwoway from "../../../assets/assetArrowTwoway.svg";
import orbGreen from "../../../assets/orbGreen.svg";
import assetMarket from "../../../assets/assetMarket.svg";
import Button from "@material-ui/core/Button/Button";
import assetArrowBlue from "../../../assets/assetArrowBlue.svg";
import {Link} from "react-router-dom";

import styles from './tradestyles';

class Trade extends Component{

    render() {

        const {classes} = this.props;

        return (
            <Grid container spacing={3}>
                <Grid item xs>
                    <Grid
                        container
                        direction="column"
                        justify="flex-start"
                        alignItems="flex-start"
                    >
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.tradeHeading}>
                                    TRADE
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.tradeContentRightStyle}>
                                    If the POOL PRICE is different to MARKET PRICE - this is your opportunity to trade.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1} >
                            </Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.tradeContentRightStyle}>
                                    You can buy cheap assets at a DISCOUNT or sell them at a PREMIUM to the market.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.tradeContentRightStyle}>
                                    The first to trade wins the opportunity.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '2.80em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1} style={{ padding: '1em' }}>
                            </Grid>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1} style={{ padding: '1em' }}>
                            </Grid>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1} style={{ padding: '1em' }}>
                            </Grid>
                            <Grid item xs={4}>
                            </Grid>
                            <Grid item xs={2}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={1}>
                            <Grid item xs style={{ padding: '1em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}></Grid>
                            <Grid item xs={1}>
                                <Button className={classes.buttonDisabled} component={Link} to="/bepswap/pool">
                                    <Grid container spacing={2}>
                                        <Grid item xs = {1}/>
                                        <Grid item >
                                            <Typography gutterBottom variant="body1" className={classes.tradeBackButtonStyle}>
                                                BACK</Typography>
                                        </Grid>
                                    </Grid>
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item  xs={4}>
                    <Grid
                        container
                        direction="column"
                        justify="flex-start"
                        alignItems="flex-start"
                    >
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '3em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '3em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '2.5em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '2.5em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '3em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={8}>
                            <Grid item xs ></Grid>
                            <Grid item xs >
                                <Grid
                                    container
                                    direction="column"
                                    justify="flex-start"
                                    alignItems="flex-start"
                                >
                                    <Grid container spacing={3}>
                                        <Grid item  xs={1} >
                                            <Typography gutterBottom variant="body1" className={classes.avatarContentPool}>POOL</Typography>
                                        </Grid>
                                        <Grid item xs={2} >
                                        </Grid>
                                    </Grid>
                                    <Grid item xs>
                                        <Avatar src={orbGreen} style={{height: 92, width: 92}} />
                                    </Grid>
                                    <Grid item xs={1}></Grid>
                                    <Grid item xs={1}>
                                        <Typography gutterBottom variant="body1" className={classes.avatarContentPrice}>$1.00</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs >
                                <Grid
                                    container
                                    direction="column"
                                    justify="flex-start"
                                    alignItems="flex-start"
                                >
                                    <Grid container spacing={3}>
                                        <Grid item  xs={1} style={{ padding: 25}}>

                                        </Grid>
                                        <Grid item xs={2} >
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={1} >
                                    </Grid>
                                    <Grid item xs={1}>
                                        <Grid container spacing={2}>
                                            <Grid item xs={1} style={{ padding: 3 }}>
                                            </Grid>
                                            <Grid item>
                                                <Avatar src={assetArrowTwoway} className={classes.avatarArrowStyle}  />
                                            </Grid>
                                            <Grid item xs={1} style={{ padding: 3 }}>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs>

                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs >
                                <Grid
                                    container
                                    direction="column"
                                    justify="flex-start"
                                    alignItems="flex-start"
                                >
                                    <Grid container spacing={3}>
                                        <Grid item  xs={1} style={{ padding: 25}}>

                                        </Grid>
                                        <Grid item xs={2} >
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={1} >
                                    </Grid>
                                    <Grid item xs={1}>
                                        <Grid container spacing={2}>
                                            <Grid item xs={1} style={{ padding: 3 }}>
                                            </Grid>
                                            <Grid item>
                                                <Avatar src={assetUser} className={classes.avatarTradeUserStyle}  />
                                            </Grid>
                                            <Grid item xs={1} style={{ padding: 3 }}>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs>

                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs >
                                <Grid
                                    container
                                    direction="column"
                                    justify="flex-start"
                                    alignItems="flex-start"
                                >
                                    <Grid container spacing={3}>
                                        <Grid item  xs={1} style={{ padding: 25}}>

                                        </Grid>
                                        <Grid item xs={2} >
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={1} >
                                    </Grid>
                                    <Grid item xs={1}>
                                        <Grid container spacing={2}>
                                            <Grid item xs={1} style={{ padding: 3 }}>
                                            </Grid>
                                            <Grid item>
                                                <Avatar src={assetArrowTwoway} className={classes.avatarArrowTwoWayStyle}  />
                                            </Grid>
                                            <Grid item xs={1} style={{ padding: 3 }}>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs>

                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs >
                                <Grid
                                    container
                                    direction="column"
                                    justify="flex-start"
                                    alignItems="flex-start"
                                >
                                    <Grid container spacing={3}>
                                        <Grid item  xs={1} >
                                            <Typography gutterBottom variant="body1" className={classes.avatarContentPool}>MARKET</Typography>
                                        </Grid>
                                        <Grid item xs={2} >
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={1} >
                                    </Grid>
                                    <Grid item xs={1}>
                                        <Grid container spacing={2}>
                                            <Grid item xs={1} style={{ padding: 3 }}>
                                            </Grid>
                                            <Grid item>
                                                <Avatar src={assetMarket} className={classes.avatarMarketStyle} />
                                            </Grid>
                                            <Grid item xs={1} style={{ padding: 10 }}>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs>
                                        <Typography gutterBottom variant="body1" className={classes.avatarContentPrice}>$1.20</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: 30 }}>
                            </Grid>
                            <Grid item xs style={{ padding: 30 }}>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item xs>
                    <Grid
                        container
                        direction="column"
                        justify="flex-start"
                        alignItems="flex-start"
                    >
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '3em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '3em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '2.5em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '2.5em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '3em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.tradeContentRightStyle}>
                                    If the POOL PRICE is different to MARKET PRICE - this is your opportunity to trade.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1} >
                            </Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.tradeContentRightStyle}>
                                    You can buy cheap assets at a DISCOUNT or sell them at a PREMIUM to the market.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.tradeContentRightStyle}>
                                    The first to trade wins the opportunity.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1} style={{ padding: '1em' }}>
                            </Grid>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={1}>
                            <Grid item xs style={{ padding: '1em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={5}>
                            <Grid item xs={3} style={{ padding: '1em' }}>
                            </Grid>
                            <Grid item xs={2} >
                            </Grid>
                            <Grid item xs={2} >
                            </Grid>
                            <Grid item xs={2}>
                                <Button className={classes.buttonSwap} component={Link} to="/finish">
                                    <Grid item>
                                        <Typography gutterBottom variant="body1" className={classes.tradeButtonStyleFinish}>
                                            FINISH</Typography>
                                    </Grid>
                                    <Grid item>
                                        <Avatar alt="Blue Arrow" src={assetArrowBlue}
                                                className={classes.tradeButtonArrow}/>
                                    </Grid>
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid container spacing={1}>
                            <Grid item xs style={{ padding: 10 }}>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        )
    };


}

export default (withStyles(styles, {withTheme: true})(Trade))
