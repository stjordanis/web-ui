import {Component} from "react";
import {withStyles} from "@material-ui/core";
import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid/Grid";
import Avatar from "@material-ui/core/Avatar/Avatar";
import assetUser from "../../../assets/assetUser.svg";
import assetArrowGreen from "../../../assets/assetArrowGreen.svg";
import orbGreen from "../../../assets/orbGreen.svg";
import assetArrowYello from "../../../assets/assetArrowYello.svg";
import assetArrowBlue from "../../../assets/assetArrowBlue.svg";
import Button from "@material-ui/core/Button/Button";
import {Link, withRouter} from "react-router-dom";

import styles from './swapstyles';

class Swap extends Component {

    render() {

        const {classes} = this.props;

        return (
            <Grid container spacing={3}>
                <Grid item xs>
                    <Grid
                        container
                        direction="column"
                        justify="flex-start"
                        alignItems="flex-start"
                    >
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1}>
                                <Typography gutterBottom variant="body1" className={classes.swapHeading}>
                                    SWAP
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1}>
                                <Typography gutterBottom variant="body1" className={classes.swapContent}>
                                    You swap assets by sending them into pools
                                    containing RUNE & BEP2 tokens.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1}>
                                <Typography gutterBottom variant="body1" className={classes.swapContent}>
                                    Swaps are calculated at prices relative to the
                                    ratio of assets in the pools.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1}>
                                <Typography gutterBottom variant="body1" className={classes.swapContent}>
                                    You can swap both ways, or swap and send to
                                    someone else.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={5}>
                    <Grid
                        container
                        direction="column"
                        justify="flex-start"
                        alignItems="flex-start"
                    >
                        <Grid container spacing={3}>
                            <Grid item xs style={{ padding: 50 }}>
                            </Grid>
                            <Grid item xs style={{ padding: 50 }}>
                            </Grid>
                            <Grid item xs style={{ padding: 50 }}>
                            </Grid>
                        </Grid>
                        <Grid >
                            <Grid item xs style={{ padding: 10 }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item xs style={{ padding: 50 }}>
                            </Grid>
                            <Grid item xs style={{ padding: 50 }}>
                            </Grid>
                            <Grid item xs style={{ padding: 50 }}>
                            </Grid>
                        </Grid>
                        <Grid container alignItems="center">
                            <Grid item xs={1}></Grid>
                            <Grid item xs={2}>
                                <Avatar alt="Asset User" src={assetUser} className={classes.avatarUserStyle}/>
                            </Grid>
                            <Grid item xs={2}>
                                <Avatar alt="Arrow" src={assetArrowGreen} className={classes.avatarArrow}/>
                            </Grid>
                            <Grid item  xs={3}>
                                <Avatar alt="Round Green" src={orbGreen} className={classes.avatarOvalGreen}/>
                            </Grid>
                            <Grid item xs={2}>
                                <Avatar alt="Arrow" src={assetArrowYello}  className={classes.avatarArrow}/>
                            </Grid>
                            <Grid item  xs={2}>
                                <Avatar alt="Asset User" src={assetUser} className={classes.avatarUserStyle}/>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item xs>
                    <Grid
                        container
                        direction="column"
                        justify="flex-start"
                        alignItems="flex-start"
                    >
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: 50 }}>
                            </Grid>
                            <Grid item xs style={{ padding: 50 }}>
                            </Grid>
                            <Grid item xs style={{ padding: 50 }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: 50 }}>
                            </Grid>
                            <Grid item xs style={{ padding: 50 }}>
                            </Grid>
                            <Grid item xs style={{ padding: 50 }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}></Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.swapContent}>
                                    When you swap, you change the balances of the assets in the pool, creating a SLIP since
                                    it changes the price.
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}></Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.swapContent} >
                                    The deeper the pool, or the smaller your transaction, the less slip.
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}></Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.swapContent} >
                                    You swap assets by sending them into pools
                                    containing RUNE & BEP2 tokens.
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container >
                            <Grid item xs style={{ padding: 10 }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={8}>
                            <Grid item xs={1}></Grid>
                            <Grid item xs={3} >
                            </Grid>
                            <Grid item xs={2} >
                            </Grid>
                            <Grid item xs={2}>
                                <Button className={classes.buttonSwap} component={Link} to="/bepswap/pool">
                                    <Grid >
                                        <Typography gutterBottom variant="body1" className={classes.buttonStyle}>
                                            POOLS</Typography>
                                    </Grid>
                                    <Grid item>
                                        <Avatar alt="Blue Arrow" src={assetArrowBlue}
                                                className={classes.buttonArrowStyle}/>
                                    </Grid>
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid container >
                            <Grid item xs style={{ padding: 10 }}>
                            </Grid>
                            <Grid item xs style={{ padding: 10 }}>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        )
    };


}


export default (withStyles(styles, {withTheme: true})(withRouter(Swap)))
