import React, {Component} from 'react'
import Paper from "@material-ui/core/Paper/Paper";
import {withStyles} from "@material-ui/core";
import styles from "../../layout/HomeStyles";
import Grid from '@material-ui/core/Grid'
import Typography from "@material-ui/core/Typography/Typography";
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import NumberFormat from 'react-number-format';
import { connect } from 'react-redux';

class Poolsetup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tickerValue: '',
            tickerPriceLabel: '',
            tickerValueAmount: 0,
            tickerValueAmountLabel: '',
            tickerCommaSeparatedValue: '',
            displayTickerValue: '',
            displayRuneValue: '',
            outputLiquidityValue: 0,
            bnbPrice: 0,
            runePrice: 0,
            tickerFinalValue: 0,
            runeFinalValue: 0,
            liquidityValue: 0,
            slipValue : 0
        };
        this.handleTextChange = this.handleTextChange.bind(this);
        this.onRunePriceChange = this.onRunePriceChange.bind(this);
        this.onTickerPriceChange = this.onTickerPriceChange.bind(this);
        this.onLiquidityPriceChange = this.onLiquidityPriceChange.bind(this);
        this.onTickerValueAmountChange = this.onTickerValueAmountChange.bind(this);
    }

    handleTextChange(e) {
        e.preventDefault();
        const { name, value } = e.target;
        const tickerValueAmountLabel = "( Trade " + value.toUpperCase() + " Amount)";
        const tickerPriceLabel = "$" + value.toUpperCase() + " Price";
        this.setState({tickerPriceLabel : tickerPriceLabel});
        this.setState({tickerValueAmountLabel : tickerValueAmountLabel});
        this.setState({ [name]: value.toUpperCase() });
    }

    onRunePriceChange(e) {
        const runePrice = (this.state.liquidityValue/e.floatValue).toFixed(2);
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
        });
        this.setState({ runePrice: e.floatValue });
        this.setState({runeFinalValue: runePrice});
        this.setState({displayRuneValue: formatter.format(runePrice)})
    }

    onTickerPriceChange(e) {
        const tickerPrice = (this.state.liquidityValue/e.floatValue).toFixed(2);

        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
        });

        this.setState({ bnbPrice: e.floatValue });
        this.setState({tickerFinalValue: tickerPrice});
        this.setState({displayTickerValue: formatter.format(tickerPrice)});
        const X = ~~tickerPrice;
        const x = ~~this.state.tickerValueAmount;

        const slipValue = (x*(2*X+x)/(x+X)**2)*100;
        this.setState({slipValue: slipValue.toFixed(2)});
    }

    onLiquidityPriceChange(e) {
        this.setState({ liquidityValue: e.floatValue });
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
        });
        const runeFinalValue = (this.state.liquidityValue/this.state.runePrice).toFixed(2);
        const tickerFinalValue = (this.state.liquidityValue/this.state.bnbPrice).toFixed(2);
        this.setState({tickerFinalValue: tickerFinalValue});
        this.setState({runeFinalValue: runeFinalValue});
        this.setState({outputLiquidityValue: e.formattedValue});
        this.setState({displayRuneValue: formatter.format(runeFinalValue)})
        this.setState({displayTickerValue: formatter.format(tickerFinalValue)})
        const X = ~~this.state.tickerFinalValue;
        const x = ~~this.state.tickerValueAmount;

        const slipValue = (x*(2*X+x)/(x+X)**2)*100;
        this.setState({slipValue: slipValue.toFixed(2)});
    }

    onTickerValueAmountChange(e) {


        const X = ~~this.state.tickerFinalValue;
        const x = ~~e.value

        const slipValue = (x*(2*X+x)/(x+X)**2)*100;

        this.setState({  tickerValueAmount: e.value });
        this.setState({slipValue: slipValue.toFixed(2)});
    }

    render() {


        const {classes} = this.props;
        const {tickerValue, bnbPrice, runePrice, liquidityValue, tickerValueAmount} = this.state;

        return (
            <div >
                <form>
                <Paper className={classes.paperStyle}>
                    <Grid container spacing={3}>
                        <Grid item xs={4} alignItems={"center"}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell></TableCell>
                                    </TableRow>
                                </TableHead>
                            </Table>
                            <TableBody >
                                <TableRow>
                                </TableRow>
                                <TableRow>
                                </TableRow>
                                <TableRow>
                                </TableRow>
                                <TableRow>
                                </TableRow>
                            </TableBody>
                        </Grid>
                        <Grid item xs={4} alignItems={"center"}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>POOL SETUP</TableCell>
                                    </TableRow>
                                </TableHead>
                            </Table>
                            <TableBody >
                                <TableRow>
                                    <TextField
                                        label="Ticker"
                                        id="standard-uncontrolled"
                                        margin="normal"
                                        name="tickerValue" value={tickerValue}
                                        onChange={this.handleTextChange}
                                    />
                                </TableRow>
                                <TableRow>
                                    <Grid container >
                                    <Grid item  xs>
                                        <Typography variant={"body1"}>{this.state.tickerPriceLabel}</Typography>
                                    </Grid>
                                    <Grid item xs >
                                        <NumberFormat
                                            id="tickerValue"
                                            margin="normal"
                                            thousandSeparator={true} prefix={'$'}
                                            name="bnbPrice" value={bnbPrice}
                                            onValueChange={this.onTickerPriceChange}
                                        />
                                    </Grid>
                                    </Grid>
                                </TableRow>
                                <TableRow>
                                    <Grid container >
                                        <Grid item  xs>
                                            <Typography variant={"body1"}>$RUNE</Typography>
                                        </Grid>
                                        <Grid item xs >
                                            <NumberFormat
                                                id="tickerValue"
                                                margin="normal"
                                                thousandSeparator={true} prefix={'$'}
                                                name="runePrice" value={runePrice}
                                                onValueChange={this.onRunePriceChange}
                                            />
                                        </Grid>
                                    </Grid>
                                </TableRow>
                                <TableRow>
                                    <Grid container >
                                        <Grid item  xs>
                                            <Typography variant={"body1"}>Liquidity</Typography>
                                        </Grid>
                                        <Grid item xs >
                                            <NumberFormat
                                                id="tickerValue"
                                                margin="normal"
                                                thousandSeparator={true} prefix={'$'}
                                                name="liquidityValue" value={liquidityValue}
                                                onValueChange={this.onLiquidityPriceChange}
                                            />
                                        </Grid>
                                    </Grid>
                                </TableRow>
                                <TableRow>
                                    <Grid container >
                                        <Grid item  xs>
                                            <Typography variant={"body1"}>{this.state.tickerValueAmountLabel}</Typography>
                                        </Grid>
                                        <Grid item xs >
                                            <NumberFormat
                                                id="tickerValue"
                                                margin="normal"
                                                thousandSeparator={true} prefix={'$'}
                                                name="tickerValueAmount" value={tickerValueAmount}
                                                onValueChange={this.onTickerValueAmountChange}
                                            />
                                        </Grid>
                                    </Grid>
                                </TableRow>
                            </TableBody>
                        </Grid>
                        <Grid item xs={4} alignItems={"center"}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>OUTPUT</TableCell>
                                    </TableRow>
                                </TableHead>
                            </Table>
                            <TableBody >
                                <TableRow>

                                </TableRow>
                                <TableRow>
                                    <Typography gutterBottom variant="body1" >{this.state.tickerValue} : RUNE</Typography>
                                </TableRow>
                                <TableRow>
                                    <TableRow>
                                        <Typography gutterBottom variant="body1" >X({this.state.displayTickerValue}) : Y({this.state.displayRuneValue})</Typography>
                                    </TableRow>
                                </TableRow>
                                <TableRow>
                                    <TableRow>
                                        <Typography gutterBottom variant="body1" >{this.state.outputLiquidityValue} : {this.state.outputLiquidityValue}</Typography>
                                    </TableRow>
                                </TableRow>
                                <TableRow>
                                    <TableRow>

                                        <Typography gutterBottom variant="body1" >Trade Slip : {this.state.slipValue}%
                                            </Typography>
                                    </TableRow>
                                </TableRow>

                            </TableBody>
                        </Grid>
                        <Grid item xs={4} alignItems={"center"}>
                        </Grid>
                    </Grid>
                </Paper>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (response) => ({
    response
});

export default connect(mapStateToProps)(withStyles(styles, {withTheme: true})(Poolsetup))
