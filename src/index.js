import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import ReactDOM from 'react-dom';
import store from './store/store';
import * as serviceWorker from './serviceWorker';
import App from './App';
import './index.css';
import 'font-awesome/css/font-awesome.min.css';


ReactDOM.render(
  <div className="App">
    <Provider store={store}>
      <Router>
        <App/>
      </Router>
    </Provider>
  </div>,
document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
