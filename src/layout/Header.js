import React from 'react'
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import { withStyles } from '@material-ui/core/styles';
import logo from './../assets/logo.svg'


const styles = theme => ({
  root: {
    boxShadow: 'none',
  },
  toolbar: {
      backgroundColor: '#fff',
      paddingLeft: 32,
      paddingRight: 32,
      paddingTop: '1em',
      paddingBottom: '1em',
      [theme.breakpoints.up('md')]: {
          paddingLeft: 96,
          paddingRight: 96,
      },
  },
  grow: {
    flexGrow: 1
  },
  logo: {
    maxHeight: 19.33,
    maxWidth: 154.79,
    [theme.breakpoints.up('md')]: {
      height: 48,
    }
  },
  heading: {
    fontSize: 15,
    width: 350,
    fontWeight: 300,
    letterSpacing: .9,
    color: '#9B9B9B',
  },
  button: {
    borderRadius: 20,
    width: 179,
    backgroundColor: '#ECEEEF',
    borderColor: '#ECEEEF'
  },
  buttonText: {
    borderRadius: 20,
    fontWeight: 600,
    fontSize: 13,
    color: '#9B9B9B',
    letterSpacing: 1.86
  },
  icon: {
    fontSize: 16,
    color: 'green'
  },
});

const Header = ({classes}) => {
  return (
      <AppBar position="static" className={classes.root}>
          <Toolbar className={classes.toolbar} >
              <Grid container spacing={3}>
                  <Grid item xs>
                      <a href={"/"}> <img src={logo} className={classes.logo}  alt="bepswap logo"/></a>
                  </Grid>
                  <Grid item xs>
                      <Typography variant="body1" color={"initial"} className={classes.heading} >SWAP AND STAKE BEP2 ASSETS</Typography>
                  </Grid>
                  <Grid item xs>
                      <Button variant="outlined" disabled className={classes.button}>
                          <Typography variant="body1" className={classes.buttonText} >COMING SOON</Typography>
                      </Button>
                  </Grid>
              </Grid>
          </Toolbar>
      </AppBar>
  );
};

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Header);
